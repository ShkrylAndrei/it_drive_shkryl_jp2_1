package ru.andrei.model;

public class Message {

    private int id;
    private String message;
    private User user;

    public Message() {
    }

    public Message(int id, String message, User user) {
        this.id = id;
        this.message = message;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", user=" + user.getId() + " " + user.getName() +
                '}';
    }
}
