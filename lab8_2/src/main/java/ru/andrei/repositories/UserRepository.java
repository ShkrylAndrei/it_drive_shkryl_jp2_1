package ru.andrei.repositories;

import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository implements CrudRepository<User>{

    private Connection connection;

    private static final String SQL_SELECT_ALL = "select  * from public.user";
    private static final String SQL_SELECT_BY_ID = "select * from public.user where id =?";


    public UserRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User find(int id){
        try{
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_BY_ID);
            st.setInt(1,id);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            int userId = resultSet.getInt("id");
            String userName = resultSet.getString("name");
            User user = new User(userId,userName);

            return user;
        }catch(SQLException e){
            throw new IllegalArgumentException();
        }
    }

    public Map<Integer,User> findAll(){
        try{
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = st.executeQuery();
            Map<Integer,User> users = new HashMap<>();
            while (resultSet.next()){
                int userId = resultSet.getInt("id");
                String userName = resultSet.getString("name");
                users.put(userId,new User(userId,userName));
            }

            return  users;
        }catch(SQLException e){
            throw new RuntimeException();
        }
    }


}
