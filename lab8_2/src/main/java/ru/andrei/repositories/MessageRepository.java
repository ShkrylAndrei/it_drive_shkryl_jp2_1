package ru.andrei.repositories;

import ru.andrei.model.Message;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

public class MessageRepository implements CrudRepository<Message>{

    private Connection connection;

    private static final String SQL_SELECT_BY_ID = "select * from public.message where id=?";
    private static final String SQL_INSERT_NEW_RECORD = "insert into public.message(message,userid) values(?,?) RETURNING id";
    private static final String SQL_INSERT_NEW_RECORD_JOIN_ROOM = "insert into public.join_message_to_room(idroom,idmessage,iduser) values(?,?,?)";
    private static final String SQL_CHECK_MESSAGE ="select * from message as m " +
                                                   "join join_message_to_room as j on m.id=j.idmessage " +
                                                   "where m.message=? and j.iduser=? and j.idroom=?";
    private static final String SQL_SELECT_ALL_BY_ROOM = "select j.id,j.idroom,j.idmessage,j.iduser,m.message,r.name" +
            "from public.join_message_to_room as j" +
            "join public.message as m on j.idmessage=m.id" +
            "join public.room as r on j.idroom=r.id" +
            "where r.id=?";


    public MessageRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Message find(int id) {
        ResultSet resultSet = null;
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_BY_ID);
            st.setInt(1, id);
            resultSet = st.executeQuery();
            resultSet.next();
            int messageId = resultSet.getInt("id");
            String messageName = resultSet.getString("message");
            int userId = resultSet.getInt("userid");

            UserRepository userRepository = new UserRepository(connection);

            Message message = new Message(messageId, messageName, userRepository.find(userId));

            return message;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        finally{
            try {
                if (resultSet != null) {
                    resultSet.close();
                } else if (st != null) {
                    st.close();
                }
            }catch (SQLException e){
                e.getMessage();
            }
        }
    }

    @Override
    public Map<Integer, Message> findAll() {
        return null;
    }

    public boolean insertNewRecord(String name, int userId, int roomId) {
        boolean result = false;

        try {
            PreparedStatement st = connection.prepareStatement(SQL_INSERT_NEW_RECORD);
            st.setString(1, name);
            st.setInt(2, userId);
            ResultSet insertRowId = st.executeQuery();
            insertRowId.next();
            if (insertRowId != null) {
                result = true;
            } else {
                result = false;
            }
            int messageId = insertRowId.getInt(1);

            st = connection.prepareStatement(SQL_INSERT_NEW_RECORD_JOIN_ROOM);
            st.setInt(1,roomId);
            st.setInt(2,messageId);
            st.setInt(3,userId);
            int count = st.executeUpdate();
            if (count>0){
                result = true;
            }else{
                result = false;
            }

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    public Optional<Message> checkMessage(int idUser, int idRoom, String message,User user){
        try {
            PreparedStatement st = connection.prepareStatement(SQL_CHECK_MESSAGE);
            st.setString(1, message);
            st.setInt(2, idUser);
            st.setInt(3, idRoom);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()){
                Message m = new Message(resultSet.getInt("idmessage"),resultSet.getString("message"),user);
                return Optional.of(m);
            }else{
                return Optional.empty();
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return Optional.empty();
    }
}






