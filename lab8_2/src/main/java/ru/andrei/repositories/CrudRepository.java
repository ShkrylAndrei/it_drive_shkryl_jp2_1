package ru.andrei.repositories;

import java.util.Map;

public interface CrudRepository<T> {

    T find(int id);

    Map<Integer,T> findAll();
}
