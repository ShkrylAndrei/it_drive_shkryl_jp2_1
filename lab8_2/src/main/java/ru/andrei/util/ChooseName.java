package ru.andrei.util;

import ru.andrei.model.User;
import ru.andrei.repositories.UserRepository;

import java.sql.Connection;
import java.util.Map;
import java.util.Scanner;

public class ChooseName {

    public User choose(Connection connection) {
        System.out.println("Выберите имя из предоставленных ниже по номеру");

        UserRepository userRepository = new UserRepository(connection);
        Map<Integer, User> users = userRepository.findAll();
        for (Map.Entry<Integer, User> u : users.entrySet()) {
            System.out.println("№ " + u.getValue().getId() + " name: " + u.getValue().getName());
        }
        Scanner sc = new Scanner(System.in);
        int choose = sc.nextInt();
        return userRepository.find(choose);
    }
}
