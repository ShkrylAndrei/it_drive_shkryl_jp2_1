package ru.andrei.util;

import ru.andrei.model.Room;
import ru.andrei.repositories.RoomRepository;

import java.sql.Connection;
import java.util.Map;
import java.util.Scanner;

public class ChooseRoom {

    public Room choose(Connection connection){
        System.out.println("Выберите комнату введя choose room <имя комнаты> , список комнат представлен ниже");
        RoomRepository roomRepository = new RoomRepository(connection);
        Map<Integer,Room> mapRoom = roomRepository.findAll();
        for (Map.Entry<Integer,Room> m : mapRoom.entrySet()){
            System.out.println("id = "+m.getValue().getId()+" "+m.getValue().getName());
        }

        Scanner sc = new Scanner(System.in);
        int choose = sc.nextInt();
        return roomRepository.find(choose);
    }
}
