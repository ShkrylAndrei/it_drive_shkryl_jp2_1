package ru.andrei.util;

public class CaseExit {

    public static boolean canExit(String str){
        boolean result = false;
        String[] userCommand = str.split(" ");
        if (userCommand[0].equals("exit")){
            if (userCommand[1].equals("from")){
                result = true;
            }
        }

        return result;
    }
}
