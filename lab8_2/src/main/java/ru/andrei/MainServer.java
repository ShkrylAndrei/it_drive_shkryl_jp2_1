package ru.andrei;

import java.io.IOException;

public class MainServer {
    public static void main(String[] args) {
        EchoSocketServer echoServerSocket = new EchoSocketServer();
        int port = 7777;


        if (args.length > 0) {
            if (args[0].split("=")[0].equals("--port")) {
                port = Integer.parseInt(args[0].split("=")[1]);
                System.out.println("Сервер будет слушать на " + port + " порту, заданном в коммандной строке");
            }
        } else {
            System.out.println("Порт прослушки не задан, по умолчанию сервер слушает на 7777 порту");
        }

        try {
            echoServerSocket.start(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


