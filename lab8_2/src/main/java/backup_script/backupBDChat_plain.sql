--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-06-30 20:49:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2845 (class 0 OID 57472)
-- Dependencies: 209
-- Data for Name: join_message_to_room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.join_message_to_room (id, idroom, idmessage, iduser) FROM stdin;
1	1	1	1
2	2	2	2
3	3	3	3
4	3	4	4
5	3	13	1
6	3	14	1
7	1	15	4
8	3	16	1
9	3	17	1
10	3	18	2
11	3	19	2
12	3	20	2
13	1	21	1
14	1	22	1
15	1	23	1
16	1	24	1
17	1	25	1
18	1	26	1
19	1	27	1
20	1	28	1
21	1	29	1
22	1	30	1
23	1	31	1
24	1	32	1
25	1	33	1
26	1	34	1
27	1	35	1
\.


--
-- TOC entry 2841 (class 0 OID 57454)
-- Dependencies: 205
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id, message, userid) FROM stdin;
1	Сообщение от Андрея	1
2	Сообщение от Маши	2
3	Сообщение от Марселя	3
4	Сообщение от Ивана	4
5	Тест	1
6	message1	1
7	test2	1
8	Тест	1
9	Тест	1
10	Тест	1
11	Тест	1
12	Тест	1
13	Тест	1
14	Тест777	1
15	сообщение от Ивана(тест)	4
16	тест от Андрея	1
17		1
18	тест	2
19	тест111	2
20	тест222	2
21	andrei test	1
22	andrei test2	1
23	andrei test3	1
24	test andrei 4	1
25	test andrei 5	1
26	test 10	1
27	test 11	1
28	exit from 1	1
29	test 12	1
30	test 13	1
31	test 14	1
32	exit from 1	1
33	test 15	1
34	test 16	1
35	test 17	1
\.


--
-- TOC entry 2839 (class 0 OID 57445)
-- Dependencies: 203
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.room (id, name) FROM stdin;
1	комната1
2	комната2
3	комната3
\.


--
-- TOC entry 2843 (class 0 OID 57463)
-- Dependencies: 207
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, name) FROM stdin;
1	andrei
2	masha
3	marsel
4	ivan
\.


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 208
-- Name: join_message_to_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.join_message_to_room_id_seq', 27, true);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 204
-- Name: newtable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.newtable_id_seq', 35, true);


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 202
-- Name: room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.room_id_seq', 3, true);


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 206
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 4, true);


-- Completed on 2020-06-30 20:49:40

--
-- PostgreSQL database dump complete
--

