package ru.andrei.servlets;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.Room;
import ru.andrei.repositories.RoomRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;

@WebServlet("/allRoom")
public class AllRoomServlet extends HttpServlet {
    private DataSource dataSource;
    private RoomRepository roomRepository;


    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        roomRepository = springContext.getBean(RoomRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<Integer, Room> mapRoom = null;
        try {
            roomRepository.setConn(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        mapRoom = roomRepository.findAll();

        StringBuilder sbRoom = new StringBuilder();
        for (Map.Entry<Integer, Room> m : mapRoom.entrySet()) {
            sbRoom.append("<p>" + "id = " + m.getValue().getId() + " "
                                          + m.getValue().getName() + "</p>");
        }

        PrintWriter writer = response.getWriter();
        writer.println("<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>List All Room</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Hello, User</h1>\n" +
                sbRoom +
                "<p><a href='/addRoom'>Add room</a></p>\n"+
                "</body>\n" +
                "</html>");
    }
}