package ru.andrei.servlets;

import org.springframework.context.ApplicationContext;
import ru.andrei.repositories.RoomRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addRoom")
public class AddRoom extends HttpServlet {
    private DataSource dataSource;
    private RoomRepository roomRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        roomRepository = springContext.getBean(RoomRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("inside");
        response.sendRedirect("html\\addRoom");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String roomName = request.getParameter("room");

        try {
            roomRepository.setConn(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        roomRepository.insertNewRecord(roomName);

        response.sendRedirect("/allRoom");
    }
}
