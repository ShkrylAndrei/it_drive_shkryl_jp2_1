package ru.andrei.repositories;

import org.springframework.stereotype.Service;
import ru.andrei.model.Room;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class RoomRepository {
    private Connection conn;

    private static final String SQL_SELECT_ALL = "select * from room";

    private static final String SQL_SELECT_BY_ID = "select r.id as roomid,r.name as roomname from public.room as r" +
            " where r.id=?";

    private static final String SQL_INSERT_NEW_RECORD = "insert into room(name) values(?)";

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public void insertNewRecord(String name) {
        boolean result = false;

        try {
            PreparedStatement st = conn.prepareStatement(SQL_INSERT_NEW_RECORD);
            st.setString(1, name);
            st.execute();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    public Room find(int id) {
        try {
            PreparedStatement st = conn.prepareStatement(SQL_SELECT_BY_ID);
            System.out.println(st);
            st.setInt(1, id);
            System.out.println(st);
            ResultSet rs = st.executeQuery();
            Room room = new Room();
            rs.next();
            room.setId(rs.getInt("roomid"));
            room.setName(rs.getString("roomname"));

            return room;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public Map<Integer, Room> findAll() {
        Map<Integer, Room> mapRoom = new HashMap<>();
        try {
            PreparedStatement st = conn.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                mapRoom.put(id, find(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapRoom;
    }
}
