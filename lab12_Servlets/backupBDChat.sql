--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-11-01 18:27:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2876 (class 0 OID 24615)
-- Dependencies: 210
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items (id, title, price) FROM stdin;
1	Milk	15
\.


--
-- TOC entry 2869 (class 0 OID 16399)
-- Dependencies: 203
-- Data for Name: join_message_to_room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.join_message_to_room (id, idroom, idmessage, iduser) FROM stdin;
1	1	1	1
2	2	2	2
3	3	3	3
4	3	4	4
5	3	13	1
6	3	14	1
7	1	15	4
8	3	16	1
9	3	17	1
10	3	18	2
11	3	19	2
12	3	20	2
13	1	21	1
14	1	22	1
15	1	23	1
16	1	24	1
17	1	25	1
18	1	26	1
19	1	27	1
20	1	28	1
21	1	29	1
22	1	30	1
23	1	31	1
24	1	32	1
25	1	33	1
26	1	34	1
27	1	35	1
28	1	36	1
29	1	37	2
30	1	38	1
1	1	1	1
2	1	2	1
3	1	3	2
4	1	4	1
5	1	5	1
6	1	6	1
7	1	7	1
\.


--
-- TOC entry 2871 (class 0 OID 16405)
-- Dependencies: 205
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id, message, userid) FROM stdin;
1	Сообщение от Андрея	1
2	Сообщение от Маши	2
3	Сообщение от Марселя	3
4	Сообщение от Ивана	4
5	Тест	1
6	message1	1
7	test2	1
8	Тест	1
9	Тест	1
10	Тест	1
11	Тест	1
12	Тест	1
13	Тест	1
14	Тест777	1
15	сообщение от Ивана(тест)	4
16	тест от Андрея	1
17		1
18	тест	2
19	тест111	2
20	тест222	2
21	andrei test	1
22	andrei test2	1
23	andrei test3	1
24	test andrei 4	1
25	test andrei 5	1
26	test 10	1
27	test 11	1
28	exit from 1	1
29	test 12	1
30	test 13	1
31	test 14	1
32	exit from 1	1
33	test 15	1
34	test 16	1
35	test 17	1
36	test18	1
37	test19	2
38	test 20	1
1	test 21	1
2	test 21	1
3	test 22	2
4	1111	1
5	111	1
6	333	1
7	1	1
\.


--
-- TOC entry 2872 (class 0 OID 16411)
-- Dependencies: 206
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.room (name, id) FROM stdin;
комната1	1
комната2	2
комната3	3
комната_тест1	4
\.


--
-- TOC entry 2880 (class 0 OID 24637)
-- Dependencies: 214
-- Data for Name: u; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.u (id, name, details_id) FROM stdin;
1	Andrei	1
\.


--
-- TOC entry 2874 (class 0 OID 16417)
-- Dependencies: 208
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, name) FROM stdin;
1	andrei
2	masha
3	marsel
4	ivan
\.


--
-- TOC entry 2878 (class 0 OID 24626)
-- Dependencies: 212
-- Data for Name: users_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_details (id, email, city) FROM stdin;
1	test@test.ru	Togliatti
\.


--
-- TOC entry 2894 (class 0 OID 0)
-- Dependencies: 209
-- Name: items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_id_seq', 1, true);


--
-- TOC entry 2895 (class 0 OID 0)
-- Dependencies: 202
-- Name: join_message_to_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.join_message_to_room_id_seq', 7, true);


--
-- TOC entry 2896 (class 0 OID 0)
-- Dependencies: 204
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_id_seq', 7, true);


--
-- TOC entry 2897 (class 0 OID 0)
-- Dependencies: 215
-- Name: room_id1_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.room_id1_seq', 4, true);


--
-- TOC entry 2898 (class 0 OID 0)
-- Dependencies: 213
-- Name: u_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.u_id_seq', 1, true);


--
-- TOC entry 2899 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 1, false);


--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 211
-- Name: users_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_details_id_seq', 1, true);


-- Completed on 2020-11-01 18:27:42

--
-- PostgreSQL database dump complete
--

