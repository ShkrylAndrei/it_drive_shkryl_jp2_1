/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.andrei.lab2;

import com.beust.jcommander.JCommander;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.FileSystems;
import java.nio.file.Path;

        

// file_size_output -> fileSize
/**
 *
 * @author Admin
 */

public class WorkWithFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Path path = FileSystems.getDefault().getPath(".");
        //System.out.println(path);
        //System.out.println(path.getParent());
        if (args.length== 0 ){
          File folder = new File("C://Windows");
        
          String[] files = folder.list(new FilenameFilter() {
 
            @Override public boolean accept(File folder, String name) {
                return name.endsWith(".exe");
            }
            
          });
        
          long file_Size_Output = 0;
         for ( String fileName : files ) {
            File file_Size = new File("C://Windows//" + fileName);
            if(file_Size.exists()){
                file_Size_Output=file_Size.length();
            }
            System.out.println("File: " + fileName+" Size "+file_Size_Output+" byte");
        }
      }else{
            Arguments arguments=new Arguments();
            JCommander.newBuilder().addObject(arguments)
            .build()
            .parse(args);
            
            
            File folder = new File(arguments.path_To_Dir);
        
             String[] files = folder.list(new FilenameFilter() {
 
            @Override public boolean accept(File folder, String name) {
                return name.endsWith(".exe");
            }
            
          });
        
          long file_Size_Output=0;
           for ( String fileName : files ) {
            File file_Size = new File(arguments.path_To_Dir+fileName);
            if(file_Size.exists()){
                file_Size_Output=file_Size.length();
            }
            System.out.println("File: " + fileName+" Size "+file_Size_Output+" byte");
      }
     }
    }//end main
    
}//end class
