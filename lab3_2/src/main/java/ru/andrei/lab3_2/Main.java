package ru.andrei.lab3_2;

import com.beust.jcommander.JCommander;
import ru.andrei.lab3.FileInformation;
import ru.andrei.lab3.FileInformationApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Пока оставил на всякий случай, потом удалю из этого проекта
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Необходимо задать аргумент в формте --p=имя папки  , если папок несколько то разделите их ;");
        } else {

            //Отладочная информация
            for (String a : args) {
                System.out.println(a);
            }

            FileInformationApi process = new FileInformationApi();
            Map<String, List<FileInformation>> result = new HashMap<>();

            Arguments arguments = new Arguments();
            JCommander.newBuilder().addObject(arguments)
                    .build()
                    .parse(args);

            List<String> path_Dir = new ArrayList<>();
            try {
                for (String retval : arguments.path_To_Dir.split(";")) {
                    path_Dir.add(retval);
                }

            } catch (Exception e) {
                System.out.println("Возникло исключение, проверьте корректность входных данных");
                e.printStackTrace();
            }

            result = process.process(path_Dir);

            //Выводим Map на экран
            for (Map.Entry<String, List<FileInformation>> entry : result.entrySet()) {
                System.out.println("Для каталога " + entry.getKey());
                int i = 1;
                for (FileInformation fi : entry.getValue()) {
                    System.out.println(i + " имя:" + fi.fileName + " размер:" + fi.sizeFile);
                    i++;
                }
            }//end for entry

        }
    }
}
