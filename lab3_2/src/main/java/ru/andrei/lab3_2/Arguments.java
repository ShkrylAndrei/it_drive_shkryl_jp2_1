package ru.andrei.lab3_2;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
/**
 *
 * @author Admin
 */
//Пока оставил на всякий случай, потом удалю из этого проекта
@Parameters(separators="=")
public class Arguments {
    @Parameter(names={"--p"})
    String path_To_Dir;
}
