package ru.andrei.lab3;

public class Main {
    public static void main(String[] args) {
        if (args.length== 0 ) {
            System.out.println("Необходимо задать аргумент в формте -p=имя папки");
        }
        else{
            Obrabotka obrabotka = new Obrabotka();
            obrabotka.obrabotka();
        }
    }
}
