package ru.andrei.lab3;

import com.beust.jcommander.JCommander;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileInformationApi {

    public Map<String, List<FileInformation>> process(List<String> path_Dir) {

        System.out.println(path_Dir);

        Map<String, List<FileInformation>> map_list_file = new HashMap<>();

        List<My_thread> listThread = new ArrayList<>();
        //Обработка списка каталогов path_Dir
        for (String folder : path_Dir) {
            File f = new File(folder);

            List<FileInformation> l = new ArrayList<>();

            listThread.add(new My_thread(folder, f, l));


        }
        for (My_thread my_thread : listThread) {
            my_thread.start();
            System.out.println("Поток " + my_thread.getName() + " начал выполнение");
        }

        for (My_thread my_thread : listThread) {
            try {
                my_thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (My_thread my_thread : listThread) {
            map_list_file.put(my_thread.string_folder, my_thread.list_file_information);
        }


        return map_list_file;

    }

}
