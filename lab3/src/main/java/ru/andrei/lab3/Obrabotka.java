package ru.andrei.lab3;

import com.beust.jcommander.JCommander;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;

public class Obrabotka {
    public Map<String, List<FileInformation>> Obrabotka()

            Arguments arguments=new Arguments();
                JCommander.newBuilder().addObject(arguments)
                .build()
                .parse(args);


            File folder = new File(arguments.path_To_Dir);

            String[] files = folder.list(new FilenameFilter() {

            @Override
            public boolean accept(File folder, String name) {
                return name.endsWith(".exe");
            }

            });

            long file_Size_Output=0;
               for ( String fileName : files )

            {
            File file_Size = new File(arguments.path_To_Dir + fileName);
            if (file_Size.exists()) {
                file_Size_Output = file_Size.length();
            }
            System.out.println("File: " + fileName + " Size " + file_Size_Output + " byte");
    }
}
