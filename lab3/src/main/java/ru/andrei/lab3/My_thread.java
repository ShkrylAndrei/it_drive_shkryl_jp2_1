package ru.andrei.lab3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class My_thread extends Thread {
    File folder;
    List<FileInformation> list_file_information;
    String string_folder;


    public My_thread(String string_folder, File folder, List<FileInformation> list_file_information) {
        this.folder = folder;
        this.list_file_information = list_file_information;
        this.string_folder = string_folder;
    }

    @Override
    public void run() {
        list_file_information = listFilesForFolder(folder);
    }

    public List<FileInformation> listFilesForFolder(File folder) {

        List<FileInformation> list_File_Information = new ArrayList<>();

        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {

            } else {
                FileInformation fileInformation = new FileInformation();
                fileInformation.fileName = fileEntry.getName();
                fileInformation.sizeFile = fileEntry.length();
                list_File_Information.add(fileInformation);
                //Отладочная информация
                //System.out.print("Файл: " + fileEntry.getName());
                //System.out.println(" размер " + (double) fileEntry.length() / 1024 + " kb");
            }

        }
        return list_File_Information;
    }
}
