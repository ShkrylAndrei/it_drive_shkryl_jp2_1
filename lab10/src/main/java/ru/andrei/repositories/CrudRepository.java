package ru.andrei.repositories;

import ru.andrei.models.Course;

import java.util.List;
import java.util.Map;

public interface CrudRepository<T> {
    T find(Integer id);

    Map<Long, T> findAll();
}