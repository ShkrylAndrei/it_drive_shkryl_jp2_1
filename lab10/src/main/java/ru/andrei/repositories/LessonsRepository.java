package ru.andrei.repositories;

import ru.andrei.models.Course;
import ru.andrei.models.Lesson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LessonsRepository implements CrudRepository {
    private Connection connection;

    private static final String SQL_SELECT_ALL = "select  *,l.id as l_id,c.id as c_id from lesson l left join course c on l.course_id = c.id";
    private static final String SQL_SELECT_BY_ID = "select * from lesson where id =?";
    private static final String SQL_SELECT_COURSE = "select * from course where id=?";

    public LessonsRepository(Connection connection) {
        this.connection = connection;
    }

    public Lesson find(Integer id) {
        try {
            PreparedStatement prSt = connection.prepareStatement(SQL_SELECT_BY_ID);
            prSt.setInt(1,id);
            ResultSet resultSet = prSt.executeQuery();
            resultSet.next();
            int lessonId = resultSet.getInt("id");
            String lessonName = resultSet.getString("name");
            int courseId = resultSet.getInt("course_id");
            Lesson lesson = new Lesson();
            lesson.setId(lessonId);
            lesson.setName(lessonName);

            PreparedStatement prStCourse = connection.prepareStatement(SQL_SELECT_COURSE);
            prStCourse.setInt(1,courseId);
            ResultSet resultSetCourse = prStCourse.executeQuery();
            Course course = new Course();
            while (resultSetCourse.next()) {
                int new_courseId = resultSetCourse.getInt("id");
                String new_courseTitle = resultSetCourse.getString("title");
                course.setId(new_courseId);
                course.setTitle(new_courseTitle);
                List<Lesson> listLesson = new ArrayList<Lesson>();
                listLesson.add(lesson);
                course.setListLesson(listLesson);
                lesson.setCourse(course);
            }
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public Map<Long,Lesson> findAll() {
        Map<Long, Lesson> lessons = new HashMap<Long, Lesson>();
        try {
            PreparedStatement prSt = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = prSt.executeQuery();

            Map<Long,Course> courses = new HashMap<Long, Course>();
            while (resultSet.next()) {
                try {
                       if (!courses.containsKey(resultSet.getLong("c_id"))) {
                           courses.put(resultSet.getLong("c_id"),new Course(resultSet.getInt("c_id"),resultSet.getString("title"),new ArrayList<Lesson>()));
                       }
                        Lesson newLesson = new Lesson(resultSet.getInt("l_id"), resultSet.getString("name"), courses.get(resultSet.getLong("c_id")));
                        lessons.put((long) newLesson.getId(), newLesson);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.getMessage();
            throw new IllegalArgumentException(e);
        }
        return lessons;
    }
}
