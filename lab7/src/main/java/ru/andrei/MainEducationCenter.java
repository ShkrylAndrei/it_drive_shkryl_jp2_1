package ru.andrei;

import ru.andrei.models.Course;
import ru.andrei.models.Lesson;
import ru.andrei.repositories.CourseRepository;
import ru.andrei.repositories.LessonsRepository;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MainEducationCenter {
    private static String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static String DB_USER = "postgres";
    private static String DB_PASSWORD = "1";

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL,DB_USER,DB_PASSWORD);

        CourseRepository courseRepository = new CourseRepository(connection);

        System.out.println("Вывод курса по id=1");
        Course course1 = courseRepository.find(1);
        System.out.println(course1);
        System.out.println("------------");
        System.out.println("Вывод всех курсов");
        Map<Long,Course> mapCourse = courseRepository.findAll();
        System.out.println(mapCourse);
        System.out.println("------------");

        LessonsRepository lessonsRepository = new LessonsRepository(connection);

        System.out.println("Вывод урока по id=1");
        Lesson lesson1 = lessonsRepository.find(1);
        System.out.println(lesson1);
        System.out.println("------------");
        System.out.println("Вывод всех уроков");
        Map<Long,Lesson> mapLesson = lessonsRepository.findAll();
        System.out.println(mapLesson);
    }
}
