package ru.andrei.models;

import java.util.List;

public class Course {
    private int id;
    private String title;
    private List<Lesson> listLesson;

    public Course() {
    }

    public Course(int id, String title, List<Lesson> listLesson) {
        this.id = id;
        this.title = title;
        this.listLesson = listLesson;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Lesson> getListLesson() {
        return listLesson;
    }

    public void setListLesson(List<Lesson> listLesson) {
        this.listLesson = listLesson;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", listLesson=" + listLesson +
                '}';
    }
}
