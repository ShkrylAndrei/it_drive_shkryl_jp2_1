package ru.andrei.repositories;

import ru.andrei.models.Course;
import ru.andrei.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseRepository implements CrudRepository {
    private Connection connection;

    private static final String SQL_SELECT_ALL = "select  *, c.id as c_id,l.id as l_id from course c left join lesson l on l.course_id = c.id";
    private static final String SQL_SELECT_LESSON = "select * from lesson where course_id=";
    private static final String SQL_SELECT_BY_ID = "select * from course where id =";


    public CourseRepository(Connection connection) {
        this.connection = connection;
    }

    public Course find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            int courseId = resultSet.getInt("id");
            String courseTitle = resultSet.getString("title");
            Course course = new Course();
            course.setId(courseId);
            course.setTitle(courseTitle);

            Statement statementLeson = connection.createStatement();
            ResultSet resultSetLeson = statement.executeQuery(SQL_SELECT_LESSON + courseId);
            List<Lesson> listLesson = new ArrayList<Lesson>();
            while (resultSetLeson.next()) {
                Lesson lesson = new Lesson();
                int lessonId = resultSetLeson.getInt("id");
                String lessonName = resultSetLeson.getString("name");
                lesson.setId(lessonId);
                lesson.setName(lessonName);
                lesson.setCourse(course);
                listLesson.add(lesson);
            }

            course.setListLesson(listLesson);
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public Map<Long, Course> findAll() {
        Map<Long, Course> courses = new HashMap<Long, Course>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {

                try {

                    if (!courses.containsKey(resultSet.getLong("c_id"))) {
                        Course newCourse = new Course(resultSet.getInt("c_id"), resultSet.getString("title"), new ArrayList());
                        courses.put((long) newCourse.getId(), newCourse);
                    }
                    Course existedCourse = courses.get(resultSet.getLong("c_id"));
                    Lesson lesson = new Lesson(resultSet.getInt("l_id"),
                            resultSet.getString("name"),
                            existedCourse);
                    existedCourse.getListLesson().add(lesson);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.getMessage();
            throw new IllegalArgumentException(e);
        }
        return courses;
    }
}
