package ru.andrei.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class  GetConnection {
    public static Connection connection;

    private static String DB_URL="jdbc:postgresql://localhost:5432/BDChat";
    private static String DB_USER="postgres";
    private static String DB_PASSWORD="1";


    public static Connection get() {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            connection = c;
        }catch (Exception e){
            e.getMessage();
        }
        return connection;
    }
}
