package ru.andrei.util;

import ru.andrei.model.User;
import ru.andrei.repositories.UserRepository;

import java.sql.Connection;
import java.util.List;
import java.util.Scanner;

public class ChooseName {

    public User choose(Connection connection){
        System.out.println("Выберите имя из предоставленных ниже по номеру");

        UserRepository userRepository = new UserRepository(connection);
        List<User> listUser = userRepository.findAll();
        for (User u : listUser){
            System.out.println("№ "+u.getId()+" name: "+u.getName());
        }
        Scanner sc = new Scanner(System.in);
        int choose = sc.nextInt();
        return userRepository.find(choose);

    }
}
