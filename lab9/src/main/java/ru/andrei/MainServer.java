package ru.andrei;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class MainServer {
    EchoSocketServer echoSocketServer;

    public MainServer(EchoSocketServer echoSocketServer) {
        this.echoSocketServer = echoSocketServer;
    }

    public static void main(String[] args) {
        int port = 7777;


        if (args.length > 0) {
            if (args[0].split("=")[0].equals("--port")) {
                port = Integer.parseInt(args[0].split("=")[1]);
                System.out.println("Сервер будет слушать на " + port + " порту, заданном в коммандной строке");
            }
        } else {
            System.out.println("Порт прослушки не задан, по умолчанию сервер слушает на 7777 порту");
        }

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        EchoSocketServer echoSocketServer = context.getBean("echoSocketServer",EchoSocketServer.class);
        try {
            echoSocketServer.start(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


