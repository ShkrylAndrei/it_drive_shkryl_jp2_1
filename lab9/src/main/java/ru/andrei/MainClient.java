package ru.andrei;

import ru.andrei.model.Message;
import ru.andrei.model.Room;
import ru.andrei.model.User;
import ru.andrei.repositories.MessageRepository;
import ru.andrei.util.CaseExit;
import ru.andrei.util.ChooseName;
import ru.andrei.util.ChooseRoom;
import ru.andrei.util.GetConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MainClient {
    private static Connection connection;
    private ChooseName chooseName;
    private User chooseUser;
    private ChooseRoom chooseRoom;
    private MessageRepository messageRepository;

    public MainClient(GetConnection connection, ChooseName chooseName, User chooseUser,
                      ChooseRoom chooseRoom, MessageRepository messageRepository){
        this.connection = GetConnection.connection;
        this.chooseName = chooseName;
        this.chooseUser = chooseUser;
        this.chooseRoom = chooseRoom;
        this.messageRepository = messageRepository;
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        int port = 7777;
        String host = "127.0.0.1";


        if (args.length == 2) {
            if (args[0].split("=")[0].equals("--serverPort")) {
                port = Integer.parseInt(args[0].split("=")[1]);
                System.out.println("Сервер будет слушать на " + port + " порту, заданном в коммандной строке");
            }
            if (args[1].split("=")[0].equals("--serverHost")) {
                host = args[1].split("=")[1];
                System.out.println("Сервер будет подключаться к хосту " + host + " , заданном в коммандной строке");
            }
        } else {
            System.out.println("Порт прослушки не задан, по умолчанию клиент подключится на 7777 порт");
            System.out.println("Хост не задан, по умолчанию клиент подключиться к 127.0.0.1");
        }


        connection = GetConnection.get();
        ChooseName chooseName = new ChooseName();
        User chooseUser = chooseName.choose(connection);

        System.out.println("Вы выбрали пользователя " + chooseUser);

        ChooseRoom chooseRoom = new ChooseRoom();
        Room room = chooseRoom.choose(connection);

        System.out.println("Вы выбрали комнату " + room.getName());

        System.out.println("Поседние 30 сообщений для данной комнаты");
        List<Message> messageList = room.getMessage().stream().sorted(new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return o1.getId() - o2.getId();
            }
        })
                .collect(Collectors.toList());
        for (Message oneMessage : messageList){
            System.out.println(oneMessage.getUser().getName()+" : "+oneMessage.getMessage());
        }

        MessageRepository messageRepository = new MessageRepository(new GetConnection());

        Socket socket = new Socket(host, port);
        PrintWriter toServer = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        System.out.println("Вводите свои сообщения ниже:");


        new Thread(() -> {
            while (true) {
                try {
                    String messageFromServer = fromServer.readLine();
                    Optional<Message> m = messageRepository.checkMessage(chooseUser.getId(), room.getId(), messageFromServer.split(">>> ")[1], chooseUser);
                    if (m.isPresent()) {
                        System.out.println("From server: "+m.get().getMessage());
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }).start();

        boolean result = false;
        while (!result) {
            String message = scanner.nextLine();
            int idUser = chooseUser.getId();
            String userName = chooseUser.getName();
            int idRoom = room.getId();
            if (CaseExit.canExit(message)){
                result = true;
            } else{
                messageRepository.insertNewRecord(message, idUser, idRoom);
                toServer.println(userName + ">>> " + message);
                toServer.flush();
            }
        }

        System.out.println("Вы вышли из программы");
    }
}

