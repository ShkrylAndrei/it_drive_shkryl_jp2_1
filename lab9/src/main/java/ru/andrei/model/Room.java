package ru.andrei.model;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private int id;
    private String name;
    private List<Message> message;

    public Room() {
    }

    public Room(int id, String name, List<Message> message) {
        this.id = id;
        this.name = name;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", message=" + message +
                '}';
    }
}
