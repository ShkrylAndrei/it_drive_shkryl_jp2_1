package ru.andrei.repositories;

import ru.andrei.model.Message;
import ru.andrei.model.Room;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoomRepository {

    private Connection connection;

    private static final String SQL_SELECT_ALL = "select * from room";


    private static final String SQL_SELECT_BY_ID = "select j.id as roomid,j.idroom,j.idmessage as idmessage,j.iduser as iduser,m.message as message,r.name as roomname,u.name as username "+
                                                   "from public.join_message_to_room as j "+
                                                   "join public.message as m on j.idmessage=m.id "+
                                                   "join public.room as r on j.idroom=r.id "+
                                                   "join public.user as u on j.iduser=u.id "+
                                                   "where r.id=?";

    public RoomRepository(Connection connection) {
        this.connection = connection;
    }

    public Room find(int id){
        try{
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_BY_ID);
            st.setInt(1,id);
            ResultSet resultSet = st.executeQuery();
            Room room = new Room();
            List<Message> listMessage = new ArrayList<>();
            boolean firstLook = true;
            while (resultSet.next()){
               if (firstLook == true){
                   room.setId(resultSet.getInt("idroom"));
                   room.setName(resultSet.getString("roomname"));
                   firstLook = false;
               }

               listMessage.add(new Message(resultSet.getInt("idmessage"),
                                           resultSet.getString("message"),
                                           new User(resultSet.getInt("iduser"),resultSet.getString("username"))));


            }

            room.setMessage(listMessage);

            return room;
        }catch(SQLException e){
            throw new IllegalArgumentException();
        }
    }

    public Map<Integer,Room> findAll(){
        Map<Integer,Room> mapRoom = new HashMap<>();
        RoomRepository roomRepository = new RoomRepository(connection);
        try {
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                mapRoom.put(id,roomRepository.find(id));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return mapRoom;
    }
}
