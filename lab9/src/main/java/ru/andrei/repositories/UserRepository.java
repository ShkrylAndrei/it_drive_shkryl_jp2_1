package ru.andrei.repositories;

import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private Connection connection;

    private static final String SQL_SELECT_ALL = "select  * from public.user";
    private static final String SQL_SELECT_BY_ID = "select * from public.user where id =?";


    public UserRepository(Connection connection) {
        this.connection = connection;
    }


    public User find(int id){
        try{
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_BY_ID);
            st.setInt(1,id);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            int userId = resultSet.getInt("id");
            String userName = resultSet.getString("name");
            User user = new User(userId,userName);

            return user;
        }catch(SQLException e){
            throw new IllegalArgumentException();
        }
    }

    public List<User> findAll(){
        try{
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet resultSet = st.executeQuery();
            List<User> listUser = new ArrayList<>();
            while (resultSet.next()){
                int userId = resultSet.getInt("id");
                String userName = resultSet.getString("name");
                listUser.add(new User(userId,userName));
            }

            return  listUser;
        }catch(SQLException e){
            throw new RuntimeException();
        }
    }


}
