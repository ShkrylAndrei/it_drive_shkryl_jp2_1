package ru.andrei.lab5;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class EchoSocketServer {

    public class SocketClient extends Thread {
        // что мы клиенту хотим отправить
        private PrintWriter output;
        // то что мы от клиента получили
        private BufferedReader input;

        public SocketClient(Socket client) {
            try {
                output = new PrintWriter(client.getOutputStream(), true);
                input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
                while (true) {
                    try {
                        String messageFromClient = input.readLine();
                        if (messageFromClient != null) {
                            System.out.println("Получили от клиента " + messageFromClient);
                            for (SocketClient anotherClient : clients) {
                                anotherClient.output.println(messageFromClient);
                                anotherClient.output.flush();
                                System.out.println("Отправлено клиенту");
                            }
                        }
                    } catch (IOException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            }
        };

    private List<SocketClient> clients;

    public void start(int port) throws IOException {
        clients = new ArrayList<>();
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket client = serverSocket.accept();
                SocketClient socketClient = new SocketClient(client);
                socketClient.start();
                clients.add(socketClient);
                System.out.println("Добавлен клиент");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}