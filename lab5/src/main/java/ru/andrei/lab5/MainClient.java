package ru.andrei.lab5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MainClient {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        int port = 7777;
        String host = "127.0.0.1";


        if (args.length == 2) {
            if (args[0].split("=")[0].equals("--serverPort")) {
                port = Integer.parseInt(args[0].split("=")[1]);
                System.out.println("Сервер будет слушать на " + port + " порту, заданном в коммандной строке");
            }
            if (args[1].split("=")[0].equals("--serverHost")) {
                host = args[1].split("=")[1];
                System.out.println("Сервер будет подключаться к хосту " + host + " , заданном в коммандной строке");
            }
        } else {
            System.out.println("Порт прослушки не задан, по умолчанию клиент подключится на 7777 порт");
            System.out.println("Хост не задан, по умолчанию клиент подключиться к 127.0.0.1");
        }

        Socket socket = new Socket(host, port);

        PrintWriter toServer = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        new Thread(() -> {
            while (true) {
                try {
                    String messageFromServer = fromServer.readLine();
                    System.out.println(messageFromServer);
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }).start();

        while (true) {
            String message = scanner.nextLine();

            toServer.println(message);
            toServer.flush();
        }
    }
}

