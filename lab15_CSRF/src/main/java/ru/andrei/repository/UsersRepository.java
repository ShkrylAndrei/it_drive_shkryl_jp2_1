package ru.andrei.repository;

import org.springframework.stereotype.Component;
import ru.andrei.model.Role;
import ru.andrei.model.SessionInfo;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(value = "usersRepositor")
public class UsersRepository {
    //language=SQL
    private static final String SQL_FIND_USER_BY_EMAIL = "select * from public.user where email = ? and password=?";
    //language=SQL
    private static final String SQL_FIND_ALL_USER = "select * from user";
    //language=SQL
    private static final String SQL_FIND_ALL_SESSION = "select s.session_value, u.email from public.session_info s inner join public.user u on u.email=s.email where u.email = ?";
    //language=SQL
    private static final String SQL_INSERT_NEW_SESSION = "insert into public.session_info(email,session_value) values(?,?)";



    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public User findUser(String login, String pass) {
        User user = new User();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_FIND_USER_BY_EMAIL);
            st.setString(1, login);
            st.setString(2, pass);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setAuthenticate(false);
                user.setRole(Role.valueOf(resultSet.getString("role")));
                //User, Admin, SuperMan
                user.setRole(Role.Admin);

                PreparedStatement st_session = connection.prepareStatement(SQL_FIND_ALL_SESSION);
                st_session.setString(1,user.getEmail());
                ResultSet resultSet_session = st_session.executeQuery();

                while (resultSet_session.next()){
                    SessionInfo sessionInfo = new SessionInfo(resultSet_session.getString("session_value"));
                    user.addElementSessionInfoList(sessionInfo);
                }
            }
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public List<User> findAllUser() {
        List<User> listUser = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_FIND_ALL_USER);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                listUser.add(new User(resultSet.getString("email"),
                        resultSet.getString("password"),
                        false,
                        ////User, Admin, SuperMan
                        //resultSet.getString("role")));
                        Role.Admin));
            }

            return listUser;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public void insertSession(String email, String session_value) {
       try {
            PreparedStatement st = connection.prepareStatement(SQL_INSERT_NEW_SESSION);
            st.setString(1, email);
            st.setString(2, session_value);
            st.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
