package ru.andrei.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class User {
    private String email;
    private String password;
    private boolean isAuthenticate;
    private Role role;
    private Set<SessionInfo> sessionInfoList = new HashSet<>();;

    public User() {
    }

    public User(String email, String password, boolean isAuthenticate, Role role) {
        this.email = email;
        this.password = password;
        this.isAuthenticate = isAuthenticate;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAuthenticate() {
        return isAuthenticate;
    }

    public void setAuthenticate(boolean authenticate) {
        isAuthenticate = authenticate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<SessionInfo> getSessionInfoList() {
        return sessionInfoList;
    }

    public void setSessionInfoList(Set<SessionInfo> sessionInfoList) {
        this.sessionInfoList = sessionInfoList;
    }

    public void addElementSessionInfoList(SessionInfo element){
        sessionInfoList.add(element);
    }
}
