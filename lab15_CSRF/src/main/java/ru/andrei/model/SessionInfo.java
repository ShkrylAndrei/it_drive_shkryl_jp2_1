package ru.andrei.model;

public class SessionInfo {
    private String session_value;

    public SessionInfo() {
    }

    public SessionInfo(String session_value) {
        this.session_value = session_value;
    }

    public String getSession_value() {
        return session_value;
    }

    public void setSession_value(String session_value) {
        this.session_value = session_value;
    }
}
