package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.SessionInfo;
import ru.andrei.model.User;
import ru.andrei.repository.UsersRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private DataSource dataSource;
    private UsersRepository usersRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        usersRepository = springContext.getBean(UsersRepository.class);
        try {
            usersRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Boolean sessionExists = session != null;
       if (request.getMethod().equals("GET") && sessionExists) {
           Boolean authAttr = (Boolean) session.getAttribute("authenticate");
           if ((authAttr!=null)&&(authAttr)) {
               usersRepository.insertSession((String) request.getSession(false).getAttribute("email"),
                       (String) request.getSession(false).getAttribute("_csrf_token"));

               SessionInfo sessionInfo = new SessionInfo((String) request.getSession(false).getAttribute("_csrf_token"));
               User user = (User)request.getSession(false).getAttribute("detail");
               user.addElementSessionInfoList(sessionInfo);
               session.setAttribute("detail", user);

               request.getRequestDispatcher("jsp/menu.jsp").forward(request, response);

           } else {
               request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
           }
       }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = usersRepository.findUser(email, password);
        if (user.getEmail()!=null && user.getEmail().equals(email) && user.getPassword().equals(password)) {
            user.setAuthenticate(true);
            HttpSession session = request.getSession(true);
            session.setAttribute("authenticate", true);
            session.setAttribute("email", email);
            session.setAttribute("role", user.getRole().toString());
            System.out.println("Зашли, сессию проставили");

            //Записываем сессию если есть что записывать
            if (request.getSession(false) != null){
                if (request.getSession(false).getAttribute("email") != null) {
                    usersRepository.insertSession((String) request.getSession(false).getAttribute("email"),
                                                  (String) request.getSession(false).getAttribute("_csrf_token"));
                }
                SessionInfo sessionInfo = new SessionInfo((String) request.getSession(false).getAttribute("_csrf_token"));
                user.addElementSessionInfoList(sessionInfo);
            }

            session.setAttribute("detail", user);

            response.setStatus(200);
            response.sendRedirect("/menu?_csrf_token="+request.getParameter("_csrf_token"));
        } else {
            response.setStatus(403);
            response.sendRedirect("html/accessDenied.html");
        }
    }
}
