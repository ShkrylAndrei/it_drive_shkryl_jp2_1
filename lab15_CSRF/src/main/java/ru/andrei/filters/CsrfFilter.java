package ru.andrei.filters;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.SessionInfo;
import ru.andrei.model.User;
import ru.andrei.repository.UsersRepository;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

public class CsrfFilter implements Filter {
    private DataSource dataSource;
    private UsersRepository usersRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        usersRepository = springContext.getBean(UsersRepository.class);
        try {
            usersRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String requestCsrf = request.getParameter("_csrf_token");
            HttpSession session = request.getSession(false);
            Boolean sessionExists = session != null;
            if (sessionExists) {
                System.out.println("в условии sessionExists метод POST");
                System.out.println("getSession(false).getId() =  " + request.getSession(false).getId());
                System.out.println("getSession(false).getAttribute(\"_csrf_token\") =" + request.getSession(false).getAttribute("_csrf_token"));
                System.out.println("requestCsrf = " + requestCsrf);

                String sessionCsrf = (String) request.getSession(false).getAttribute("_csrf_token");
                if (sessionCsrf.equals(requestCsrf)) {
                    System.out.println("POST " + requestCsrf);
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            } else {
                response.setStatus(403);
                response.sendRedirect("html/accessDenied.html");
                return;
            }
        }
        if (request.getMethod().equals("GET")) {
            String requestCsrf = request.getParameter("_csrf_token");
            HttpSession session = request.getSession(false);
            Boolean sessionExists = session != null;

            if (sessionExists && requestCsrf != null) {
                System.out.println("в условии sessionExists метод GET");
                System.out.println("getSession(false).getId() =  " + request.getSession(false).getId());
                System.out.println("getSession(false).getAttribute(\"_csrf_token\") =" + request.getSession(false).getAttribute("_csrf_token"));
                System.out.println("requestCsrf = " + requestCsrf);

                String sessionCsrf = (String) request.getSession(false).getAttribute("_csrf_token");

                System.out.println(request.getRequestURI());
                System.out.println(request.getRequestURI().regionMatches(0, "/login", 0, 6));
                if (request.getRequestURI().regionMatches(0, "/login", 0, 6)) {
                    if (sessionCsrf.equals(requestCsrf)) {
                        filterChain.doFilter(servletRequest, servletResponse);
                        return;
                    }
                }else {
                    Boolean checkCSRF = false;
                    User processingUser = (User) session.getAttribute("detail");
                    for (SessionInfo s : processingUser.getSessionInfoList()) {
                        System.out.println(s.getSession_value()+" "+requestCsrf);
                        if (s.getSession_value().equals(requestCsrf)) {
                            filterChain.doFilter(servletRequest, servletResponse);
                            return;
                        }
                    }
                }

            } else {
                System.out.println("Зашли в условие когда CSRF где-то не проставлен");
                String WhatIn = request.getRequestURI();
                String loginEquals = "/login";
                String logoutEquals = "/logout";
                String accessDeniedEquals = "/html/accessDenied";
                String faviconEquals = "/favicon.ico";
                //String emptyEquals = "/";
                boolean loginRequest = WhatIn.regionMatches(0, loginEquals, 0, 6);
                boolean logoutRequest = WhatIn.regionMatches(0, logoutEquals, 0, 7);
                boolean accessDeniedRequest = WhatIn.regionMatches(0, accessDeniedEquals, 0, 17);
                boolean faviconRequest = WhatIn.regionMatches(0, faviconEquals, 0, 12);
                //boolean emptyRequest = WhatIn.regionMatches(0, emptyEquals, 0, 1) & WhatIn.length() < 3;

                Boolean isRequestOnOpenPage = false;
                if (loginRequest || logoutRequest || accessDeniedRequest || faviconRequest) {
                    isRequestOnOpenPage = true;
                }

                if (isRequestOnOpenPage) {
                    String csrf = UUID.randomUUID().toString();
                    request.setAttribute("_csrf_token", csrf);
                    request.getSession().setAttribute("_csrf_token", csrf);
                    System.out.println("GET " + csrf);


                    filterChain.doFilter(servletRequest, servletResponse);
                    return;

                }
            }
        }

    }

    @Override
    public void destroy() {

    }
}
