package ru.andrei.filters;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.SessionInfo;
import ru.andrei.model.User;
import ru.andrei.repository.UsersRepository;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

public class NewSessionAfterLogin implements Filter {
    private DataSource dataSource;
    private UsersRepository usersRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        usersRepository = springContext.getBean(UsersRepository.class);
        try {
            usersRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String requestCsrf = request.getParameter("_csrf_token");
        HttpSession session = request.getSession(false);
        Boolean sessionExists = session != null;

        if (sessionExists) {
            Boolean auth = (Boolean) request.getSession(false).getAttribute("authenticate");
            if ((auth != null) && (auth == true)) {
                String sessionCsrf = (String) request.getSession(false).getAttribute("_csrf_token");
                if (sessionCsrf.equals(requestCsrf)) {
                    //Удаляем старую сессию из базы, чтобы ниже выдать новую
                    usersRepository.deleteSession(sessionCsrf,(String) request.getSession(false).getAttribute("email"));

                    String WhatIn = request.getRequestURI();

                    String csrf = UUID.randomUUID().toString();
                    request.setAttribute("_csrf_token", csrf);
                    request.getSession(false).setAttribute("_csrf_token", csrf);

                    usersRepository.insertSession((String) request.getSession(false).getAttribute("email"),
                            csrf);
                    SessionInfo sessionInfo = new SessionInfo((String) request.getSession(false).getAttribute("_csrf_token"));
                    User user = (User) session.getAttribute("detail");
                    user.addElementSessionInfoList(sessionInfo);
                    session.setAttribute("detail", user);
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void destroy() {

    }
}
