package ru.andrei.repository;

import org.springframework.stereotype.Component;
import ru.andrei.model.Role;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(value = "usersRepositor")
public class UsersRepository {
    //language=SQL
    private static final String SQL_FIND_USER_BY_EMAIL = "select * from public.user where email = ? and password=?";
    //language=SQL
    private static final String SQL_FIND_ALL_USER = "select * from user";

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public User findUser(String login, String pass) {
        User user = new User();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_FIND_USER_BY_EMAIL);
            st.setString(1, login);
            st.setString(2, pass);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setAuthenticate(false);
                user.setRole(Role.valueOf(resultSet.getString("role")));
                //User, Admin, SuperMan
                user.setRole(Role.Admin);
            }
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public List<User> findAllUser() {
        List<User> listUser = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_FIND_ALL_USER);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                listUser.add(new User(resultSet.getString("email"),
                        resultSet.getString("password"),
                        false,
                        ////User, Admin, SuperMan
                        //resultSet.getString("role")));
                        Role.Admin));
            }

            return listUser;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
