package ru.andrei.model;

public enum Role {
    User,
    Admin,
    SuperMan
}
