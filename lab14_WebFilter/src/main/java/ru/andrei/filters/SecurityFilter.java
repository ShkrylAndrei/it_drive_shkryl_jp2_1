package ru.andrei.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
//@WebFilter({"/admin/*", "/user/*"})
public class SecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        System.out.println("In SecurityFilter " + request.getRequestURI() + "?" + request.getQueryString());

        // берем сессию у запроса
        // берем только существующую, если сессии не было - то вернет null
        HttpSession session = request.getSession(false);

        // флаг, аутентифицирован ли пользователь
        Boolean isAuthenticated = false;

        Boolean sessionExists = session != null;
        // идет ли запрос на страницу входа или регистрации?
        String WhatIn = request.getRequestURI();
        String loginEquals = "/login";
        String logoutEquals = "/logout";
        String accessDeniedEquals="/html/accessDenied";
        String emptyEquals = "/";
        boolean loginRequest = WhatIn.regionMatches(0, loginEquals, 0, 6);
        boolean logoutRequest = WhatIn.regionMatches(0, logoutEquals, 0, 7);
        boolean accessDeniedRequest = WhatIn.regionMatches(0,accessDeniedEquals,0,17);
        boolean emptyRequest = WhatIn.regionMatches(0, emptyEquals, 0, 1) & WhatIn.length()<3;

        Boolean isRequestOnOpenPage = false;
        if (loginRequest || logoutRequest || emptyRequest || accessDeniedRequest) {
            isRequestOnOpenPage = true;
        }

        String menuEquals = "/menu";
        String infoUsersEquals = "/infoUsers";
        String timeCurrentEquals = "/timeCurrent";
        boolean menuRequest = WhatIn.regionMatches(0, menuEquals, 0, 5);
        boolean infoUsersRequest = WhatIn.regionMatches(0, infoUsersEquals, 0, 10);
        boolean timeCurrentRequest = WhatIn.regionMatches(0, timeCurrentEquals, 0, 12);

        Boolean isRequestOnInsidePage = false;
        if (menuRequest || infoUsersRequest || timeCurrentRequest) {
            isRequestOnInsidePage = true;
        }

        if (sessionExists) {
            // проверим, есть ли атрибует user?
            isAuthenticated = session.getAttribute("authenticate") != null &&
                              (boolean)session.getAttribute("authenticate")==true;
            if (session.getAttribute("authenticate") != null) {
                System.out.println("В фильтре авторизация: " + (Boolean) session.getAttribute("authenticate"));
            }
        }


        String role = null;
        if (session != null) {
            if (session.getAttribute("role") != null) {
                role = (String) request.getSession().getAttribute("role");
            }
        } else {
            role = "User";
        }
        System.out.println("Роль " + role);


        if (isAuthenticated && isRequestOnInsidePage) {
            // отдаем ему то, что он хочет
            System.out.println("В фильтре, авторизованы, ");
            //String linkWhere = isProtected(role.toString());
            System.out.println("перенаправляем пользователя на страницу внутри");

            filterChain.doFilter(request, response);

        } else if (!isAuthenticated && isRequestOnOpenPage) {
            System.out.println("перенаправляем пользователя на страницу авторизации");
            filterChain.doFilter(request, response);
        }else if (isAuthenticated && isRequestOnOpenPage) {
            System.out.println("перенаправляем пользователя на страницу выхода");
            filterChain.doFilter(request, response);
        }
    }


    @Override
    public void destroy() {

    }
}
