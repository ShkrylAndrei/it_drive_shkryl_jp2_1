package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
//        ServletContext servletContext = config.getServletContext();
//        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            System.out.println("До обнуления авторизации ");
            Boolean authenticate = (Boolean) session.getAttribute("authenticate");
            if (authenticate!=null && authenticate) {
                session.setAttribute("authenticate", false);
                System.out.println("После обнуления авторизации ");
            }
        }
        response.sendRedirect("/login");
    }
}
