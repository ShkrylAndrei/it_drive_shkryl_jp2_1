package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.User;
import ru.andrei.repository.UsersRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private DataSource dataSource;
    private UsersRepository usersRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        usersRepository = springContext.getBean(UsersRepository.class);
        try {
            usersRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/login.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = usersRepository.findUser(email, password);
        if (user.getEmail()!=null && user.getEmail().equals(email) && user.getPassword().equals(password)) {
            user.setAuthenticate(true);
            HttpSession session = request.getSession(true);
            session.setAttribute("authenticate", true);
            session.setAttribute("email", email);
            session.setAttribute("role", user.getRole().toString());
            System.out.println("Зашли, сессию проставили");
            response.setStatus(200);
            response.sendRedirect("/menu");
        } else {
            response.setStatus(403);
            response.sendRedirect("html/accessDenied.html");
        }
    }
}
