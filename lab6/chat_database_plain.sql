--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-04-12 16:16:48

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2854 (class 0 OID 16452)
-- Dependencies: 205
-- Data for Name: Chat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Chat" (id, name, date_create) FROM stdin;
1	Чат1                          	2020-04-11 00:00:00
2	Чат2                          	2019-01-12 00:00:00
3	Чат3                          	2017-09-13 00:00:00
\.


--
-- TOC entry 2856 (class 0 OID 16500)
-- Dependencies: 207
-- Data for Name: Message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Message" (id, text_message, user_id, chat_id) FROM stdin;
1	Первое сообщение                                                                                    	1	1
2	Второе сообщение                                                                                    	2	2
3	Третье сообщение                                                                                    	3	3
4	Четвертое сообщение                                                                                 	1	1
\.


--
-- TOC entry 2852 (class 0 OID 16444)
-- Dependencies: 203
-- Data for Name: User; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."User" (id, name, surname, email, password) FROM stdin;
1	Андрей                        	Шкрыль                        	ShkrylAndrei@rambler.ru       	1                             
2	Иван                          	Иванов                        	Ivanov@rambler.ru             	2                             
3	Петр                          	Петров                        	Petrov@rambler.ru             	3                             
\.


--
-- TOC entry 2858 (class 0 OID 16520)
-- Dependencies: 209
-- Data for Name: User_belongs_to_chat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."User_belongs_to_chat" (id, user_id, char_id) FROM stdin;
5	1	1
6	2	2
7	3	3
\.


--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 204
-- Name: Chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Chat_id_seq"', 3, true);


--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 208
-- Name: User_belongs_to_chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."User_belongs_to_chat_id_seq"', 7, true);


--
-- TOC entry 2870 (class 0 OID 0)
-- Dependencies: 202
-- Name: User_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."User_id_seq"', 3, true);


--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 206
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_id_seq', 4, true);


-- Completed on 2020-04-12 16:16:49

--
-- PostgreSQL database dump complete
--

