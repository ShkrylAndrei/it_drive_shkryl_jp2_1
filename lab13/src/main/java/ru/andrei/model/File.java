package ru.andrei.model;

public class File {
    private int id;
    private String filename;
    private User user;

    public File() {
    }

    public File(int id, String filename, User user) {
        this.id = id;
        this.filename = filename;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ListFiles{" +
                "id=" + id +
                ", filename='" + filename + '\'' +
                ", user=" + user +
                '}';
    }
}
