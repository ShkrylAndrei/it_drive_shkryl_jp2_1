package ru.andrei.exception;

public class InvalidLoginOrPasswordException extends Exception {
    public InvalidLoginOrPasswordException(String msg) {
        super(msg);
    }
}
