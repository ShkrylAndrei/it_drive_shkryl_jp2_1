package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.File;
import ru.andrei.model.User;
import ru.andrei.repository.FileRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/fileList")
public class FileList extends HttpServlet {
    private DataSource dataSource;
    private FileRepository fileRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        fileRepository = springContext.getBean(FileRepository.class);
        try {
            fileRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String login = "";
        String info = "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                login = cookie.getValue();
            }
            if (cookie.getName().equals("info")) {
                info = cookie.getValue();
            }
        }
        if (login.equals("")) {
            new IllegalArgumentException("такого пользователя не существует, вы не авторизованы");
        }

        User user = fileRepository.findByLogin(login);
        List<File> userFiles = fileRepository.getListFiles(user);

        PrintWriter writer = response.getWriter();
        StringBuilder sb = new StringBuilder();
        for (File f : userFiles) {
            sb.append("<a href='/detailFile?file="+f.getId()+"'>" + f.getFilename() + "</a>" + "<br/>");
        }

        writer.println("<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>Document</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Hello, User, this is list of file</h1>" +
                "<br/>"+info+"<br/>"+
                "<a href=\"/fileUpload\">Upload new page</a><br/>"+
                sb +
                "</body>\n" +
                "</html>");
    }
}
