package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.File;
import ru.andrei.repository.FileRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.sql.SQLException;

@WebServlet("/detailFile")
public class FileListDetail extends HttpServlet {
    private DataSource dataSource;
    private FileRepository fileRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        fileRepository = springContext.getBean(FileRepository.class);
        try {
            fileRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        File fileImage = null;

        Integer idFile = Integer.parseInt(request.getParameter("file"));
        if ((idFile!=null)&&(idFile>0)) {
            fileImage = fileRepository.findByIdFile(idFile);
            System.out.println("============");
            System.out.println(idFile);
            System.out.println(fileImage);
        }else{
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/html/error/fileIsNotExists.html");
            requestDispatcher.forward(request, response);
        }

//        PrintWriter writer = response.getWriter();

//        writer.println("<!doctype html>\n" +
//                "<html lang=\"en\">\n" +
//                "<head>\n" +
//                "    <title>Document</title>\n" +
//                "</head>\n" +
//                "<body>\n" +
//                "<h1>Hello, User</h1>" +
//                "<h1>" + fileImage.getFilename() + "</h1>\n" +
//                "<img src=\""+fileImage.getFilename()+"\">\n" +
//                "</body>\n" +
//                "</html>");

        java.io.File file = new java.io.File(fileImage.getFilename());
        response.setContentType("image/jpeg");
        response.setContentLength((int)file.length());
        response.setHeader("Content-Disposition", "filename=\"" + fileImage.getFilename() + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
        response.flushBuffer();
    }
}

