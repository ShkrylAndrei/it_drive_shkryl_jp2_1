package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.User;
import ru.andrei.repository.FileRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/fileUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {
    private DataSource dataSource;
    private FileRepository fileRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);

        fileRepository = springContext.getBean(FileRepository.class);
        try {
            fileRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/uploadForm.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part part = request.getPart("file");
        System.out.print(part.getSubmittedFileName() + " ");
        System.out.print(part.getContentType() + " ");
        System.out.println(part.getSize());

        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

        Date dateNow = new Date();
        String prefixDate=formatForDateNow.format(dateNow);

        String fileName = "C://temp//marsel//"+prefixDate + part.getSubmittedFileName();
       // String fileNameForBase = "C:\\temp\marsel\\"+prefixDate + part.getSubmittedFileName();
        Files.copy(part.getInputStream(), Paths.get(fileName));

        Cookie[] cookies = request.getCookies();
        String login="";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login")) {
                login = cookie.getValue();
            }
        }
        if (login.equals("")){
            new IllegalArgumentException("такого пользователя не существует, вы не авторизованы");
        }
        User user = fileRepository.findByLogin(login);
        fileRepository.insertNewFile(user,fileName);

        Cookie info = new Cookie("info","File_succesful_upload");
        response.addCookie(info);
       // RequestDispatcher requestDispatcher = request.getRequestDispatcher("/fileList");
        //requestDispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
        System.out.println("Servlet FilesUploadServlet was destoryed!");
    }
}
