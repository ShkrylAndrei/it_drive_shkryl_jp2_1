package ru.andrei.servlet;

import org.springframework.context.ApplicationContext;
import ru.andrei.model.User;
import ru.andrei.repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet("/signIn")
public class SignIn extends HttpServlet {
    private DataSource dataSource;
    private UserRepository userRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");

        dataSource = springContext.getBean(DataSource.class);
        userRepository = springContext.getBean(UserRepository.class);
        try {
            userRepository.setConnection(dataSource.getConnection());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/signIn.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String pass = request.getParameter("pass");

        User user = null;
        try {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            System.out.println("Random UUID String = " + randomUUIDString);

            user = userRepository.find(login, pass);

            if (user != null) {
                Cookie UUIDCookie = new Cookie("UUID", randomUUIDString);
                Cookie loginCookie = new Cookie("login", user.getName());
                response.addCookie(UUIDCookie);
                response.addCookie(loginCookie);
                userRepository.saveUUID(randomUUIDString, user);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("html/authPage.html");
                requestDispatcher.forward(request, response);
            } else {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("html/wrongData.html");
                requestDispatcher.forward(request, response);
            }
            System.out.println("Вы выбрали пользователя " + user.getName());
        } catch (Exception e) {
            System.out.println("Логин и пароль не верные");
            e.printStackTrace();
            request.getRequestDispatcher("html/wrongData.html").forward(request, response);
        }
    }
}
