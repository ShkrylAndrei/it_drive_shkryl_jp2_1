package ru.andrei.repository;

import org.springframework.stereotype.Service;
import ru.andrei.exception.InvalidLoginOrPasswordException;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class UserRepository {
    private Connection connection;

    private static final String SQL_SELECT_BY_LOGIN_AND_PASS = "select * from public.users where login =? and pass =?";
    private static final String SQL_SAVE_NEW_UUID = "update public.users set token=? where id=?";

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public User find(String login, String pass) throws InvalidLoginOrPasswordException {
        User user = new User();
        try {
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_BY_LOGIN_AND_PASS);
            st.setString(1, login);
            st.setString(2, pass);
            System.out.println(st);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("login"));
            } else {
                throw new InvalidLoginOrPasswordException("Wrong Login or password");
            }
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public void saveUUID(String randomUUIDString, User user) {
        try {
            PreparedStatement stUUID = connection.prepareStatement(SQL_SAVE_NEW_UUID);
            stUUID.setString(1, randomUUIDString);
            stUUID.setInt(2, user.getId());
            stUUID.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
