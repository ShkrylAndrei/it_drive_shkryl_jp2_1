package ru.andrei.repository;

import org.springframework.stereotype.Service;
import ru.andrei.model.File;
import ru.andrei.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileRepository {
    private Connection connection;

    private static final String SQL_SELECT_BY_LOGIN = "select * from public.users where login=?";
    private static final String SQL_SELECT_ALL_FILES_FOR_CHOOSE_USER = "select * from listfiles as lf join users as u on u.id=lf.iduser where u.login=?";
    private static final String SQL_INSERT_NEW_FILE = "insert into listfiles(iduser,filename) values(?,?);";
    private static final String SQL_SELECT_DETAIL_INFO_FILE = "select * from listfiles where id=?";

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public File findByIdFile(int id) {
        File file = new File();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_DETAIL_INFO_FILE);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                file.setId(resultSet.getInt("id"));
                file.setFilename(resultSet.getString("filename"));
            }
            return file;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public User findByLogin(String login) {
        User user = new User();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_BY_LOGIN);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("login"));
                user.setToken(resultSet.getString("token"));
            }
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }


    public List<File> getListFiles(User user) {
        int id;
        String fileName;
        ArrayList<File> listFile = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_FILES_FOR_CHOOSE_USER);
            st.setString(1, user.getName());
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                fileName = resultSet.getString("filename");


                listFile.add(new File(id, fileName, user));
            }
            return listFile;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    public void insertNewFile(User user, String fileName) {
        try {
            PreparedStatement st = connection.prepareStatement(SQL_INSERT_NEW_FILE);
            st.setInt(1, user.getId());
            st.setString(2, fileName);
            System.out.println(st);
            st.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
